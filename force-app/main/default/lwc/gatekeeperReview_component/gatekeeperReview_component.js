import { LightningElement,api,wire, track } from 'lwc';
//import { getSObjectValue } from '@salesforce/apex';
import getProductList from '@salesforce/apex/gatekeeperController.getProductList';
//import PRODUCT_NAME_FIELD from '@salesforce/schema/OpportunityLineItem.Product2.Name';
import { NavigationMixin } from 'lightning/navigation';

const cols = [
    { label: 'Product Name', fieldName: 'Product_Description__c' },       
    { label: 'Total Price', fieldName: 'TotalPrice',type:'currency' },
    { label: 'Billing Frequency', fieldName: 'Billing_Frequency__c' },
    { label: 'Line Item Start Date', fieldName: 'Line_Item_Start_Date__c',type:'date' },
    { label: 'Line Item End Date', fieldName: 'Line_Item_End_Date__c',type:'date' },
    { label: 'Description', fieldName: 'Description' }, 
    { label: 'Type', fieldName: 'Type__c' }
];


export default class GatekeeperReview_component extends NavigationMixin(LightningElement){
   

    @track columns=cols;
    @track ProdName;
    @track products;
    @track error; 

    @api recordId;
    @wire(getProductList,{ recordId: '$recordId' })
    wiredProducts({error,data}){
        if(data){
            this.products=data;
            /*eslint-disable no-console*/
            console.log('@@getProducts:'+JSON.stringify(data));
            console.log('Product Name on products attribute = ',this.products[0].Product2.Name);
        }
        if(error){
            this.error=error;
            /*eslint-disable no-console*/
            console.log('ERROR > No Data fetched');
        }
    }

    // get ProductName() {
    //    return this.product.data ? getSObjectValue(this.product.data, PRODUCT_NAME_FIELD) : '';
   // }

    navigateOpportunityRecordPage() {
           // Navigate to the opportunity object's home page.
         this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                recordId: '0061w000019Fx6vAAC',
                objectApiName: 'Opportunity',
                actionName: 'view'
               }
         });
    }
    navigateToHome() {
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'home'
            }
        });
    }
    navigateToWebPage() {
        // Navigate to a URL
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: 'http://salesforce.com'
            }
        },
        true
      );
    }
}