public with sharing class gatekeeperController {
    
    @AuraEnabled(cacheable=true)
    public static List<sObject> getProductList(String recordId) {

        List<OpportunityLineItem> lstLineItems =  new  List<OpportunityLineItem>([SELECT Id,Product2Id,Product2.Name,Sales_Org__c,Sales_Org_Code__c,Description,Product_Description__c,Billing_Frequency__c, ProductCode, Name, TotalPrice, Line_Item_End_Date__c, Line_Item_Start_Date__c,Type__c  FROM OpportunityLineItem where opportunityid =: recordId ]);
        return(lstLineItems); 
    }
}